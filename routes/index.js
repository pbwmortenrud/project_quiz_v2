var express = require('express');
var router = express.Router();

/* GET index page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Project Quiz V2' });
});
/* GET admin page. */
router.get('/admin', function(req, res, next) {
  res.render('admin', { title: 'Admin page' });
});

//TODO: Router /admin with POST

/* GET show quiz page. */
router.get('/showquiz', function(req, res, next) {
  res.render('quiz', { title: 'Quizzes' });
});

/* GET take a quiz page. */
router.get('/takequiz', function(req, res, next) {
  res.render('takequiz', { title: 'Quiz' });
});

module.exports = router;
